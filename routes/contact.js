var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('contact', { title: 'Contact' });
});

/* POST */
router.post('/send', function(req, res, next){
	var transporter = nodemailer.createTransport({
		service: 'Gmail',
		auth: {
			user: 'NEED USERNAME',
			pass: 'NEED PASSWORD'
		}
	});

	var mailOptions = {
		from: 'John Doe <johndoe@gmail.com>',
		to: 'Nick Stalter <nickstalter@gmail.com',
		subject: 'Website Inquiry',
		text: 'You have a new website inquiry with the following details: Name - ' + req.body.name+ '. Email - ' + req.body.email + '. Message - ' + req.body.message,
		html: '<p>You have a new website inquiry with the following details: Name - ' + req.body.name+ '. Email - ' + req.body.email + '. Message - ' + req.body.message +'</p>'
	};
	transporter.sendMail(mailOptions, function(error, info){
		if(error){
			console.log("Error: " + error);
			res.redirect('/');
		} else {
			console.log('Message Sent: ' + info.response);
			res.redirect('/');
		}
	});
});

module.exports = router;
